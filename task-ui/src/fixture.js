module.exports = [
    {
        "id": 1,
        "amount": 753.63,
        "date": "2012-02-02T00:00:00.000Z",
        "business": "Lindgren LLC",
        "name": "Credit Card Account 8708",
        "type": "deposit",
        "account": "22098550"
    },
    {
        "id": 2,
        "amount": 69.18,
        "date": "2012-02-02T00:00:00.000Z",
        "business": "Considine and Sons",
        "name": "Investment Account 4042",
        "type": "payment",
        "account": "00569976"
    },
    {
        "id": 3,
        "amount": 491.03,
        "date": "2012-02-02T00:00:00.000Z",
        "business": "Gerhold and Sons",
        "name": "Savings Account 9927",
        "type": "invoice",
        "account": "00800431"
    },
    {
        "id": 4,
        "amount": 20.72,
        "date": "2012-02-02T00:00:00.000Z",
        "business": "Pfannerstill, Schaden and Anderson",
        "name": "Auto Loan Account 5488",
        "type": "invoice",
        "account": "77586861"
    },
    {
        "id": 5,
        "amount": 263.99,
        "date": "2012-02-02T00:00:00.000Z",
        "business": "Bednar - Littel",
        "name": "Checking Account 2465",
        "type": "withdrawal",
        "account": "70940254"
    },
    {
        "id": 6,
        "amount": 553.03,
        "date": "2012-02-02T00:00:00.000Z",
        "business": "Bernier and Sons",
        "name": "Savings Account 8661",
        "type": "invoice",
        "account": "15993468"
    },
    {
        "id": 8,
        "amount": 247.08,
        "date": "2012-02-02T00:00:00.000Z",
        "business": "Johns LLC",
        "name": "Home Loan Account 5558",
        "type": "payment",
        "account": "68382271"
    },
    {
        "id": 9,
        "amount": 566.43,
        "date": "2012-02-02T00:00:00.000Z",
        "business": "Medhurst and Sons",
        "name": "Personal Loan Account 2486",
        "type": "deposit",
        "account": "10840565"
    },
    {
        "id": 9,
        "amount": 560.43,
        "date": "2012-02-02T00:00:00.000Z",
        "business": "Medhurst and Sons",
        "name": "Personal Loan Account 2486",
        "type": "deposit",
        "account": "10840565"
    },
    {
        "id": 10,
        "amount": 672.89,
        "date": "2012-02-02T00:00:00.000Z",
        "business": "Stracke Inc",
        "name": "Personal Loan Account 2820",
        "type": "withdrawal",
        "account": "45578355"
    }
]