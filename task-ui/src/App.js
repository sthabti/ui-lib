import React, { Component } from "react";
import Lib from "task-library";
import "./style.css";

class App extends Component {
  state = {
    transactions: []
  };
  componentDidMount() {
    return this.fetchTransactions()
  }

  fetchTransactions() {
    return fetch("http://localhost:3000/transactions").then(res => res.json())
      .then(transactions => {
        return this.setState({
          transactions
        });
      });
  }

  handleSortByAmount() {
    const lib = new Lib(this.state.transactions);
    this.setState({
      transactions: lib.sortByAmount()
    });
  }

  handleSearchByName(e) {
    const lib = new Lib(this.state.transactions);

    this.setState({
      transactions: lib.findByName(e.target.value)
    });
  }

  render() {
    const body = this.state.transactions.map(item => {
      return (
        <tbody key={item.id}>
          <tr>
            <td>{item.business}</td>
            <td>{item.name}</td>
            <td>{item.date}</td>
            <td>{item.amount}</td>
          </tr>
        </tbody>
      );
    });

    return (
      <div className="App">
        <h1>Transactions</h1>

        <input
          type="text"
          placeholder="search by name"
          onChange={this.handleSearchByName.bind(this)}
        />

        <button onClick={this.fetchTransactions.bind(this)}>Reset</button>

        <hr />

        <table>
          <thead>
            <tr>
              <th>Business</th>
              <th>Name</th>
              <th>Date</th>
              <th
                className="sortable amount"
                onClick={this.handleSortByAmount.bind(this)}
              >
                Amount
              </th>
            </tr>
          </thead>
          {body}
        </table>
      </div>
    );
  }
}

export default App;
