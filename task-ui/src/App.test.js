
import React from 'react';
import ReactDOM from 'react-dom';
import Lib from 'task-library'
import App from './App';
import fixture from './fixture'


let fetchStub


it('renders without crashing', () => {
    const app = shallow(<App />)
    expect(app.find('.App')).to.be.present()
});

it('should fetch transactions snd populate the state', () => {

    const spyComponentDidMount = sinon.spy(App.prototype, 'componentDidMount')

    const app = shallow(<App />);

    return app.instance().fetchTransactions().then(data => {
        expect(spyComponentDidMount).to.have.been.called
        expect(fetchStub).to.have.been.calledWith('http://localhost:3000/transactions')
        expect(app.state().transactions).to.eql(fixture);
    });
});

it('should use library to sort transactions by amount', () => {
    const spyHandleSortByAmount = sinon.spy(App.prototype, 'handleSortByAmount')
    const libSortSpy = sinon.spy(Lib.prototype, 'sortByAmount')
    const app = shallow(<App />);

    app.setState({ transactions: fixture })

    app.find('.sortable.amount').simulate('click');

    expect(spyHandleSortByAmount).to.have.been.called
    expect(libSortSpy).to.have.been.called

})


it('should use library to filter transactions by name', () => {
    const handleSearchByNameSpy = sinon.spy(App.prototype, 'handleSearchByName')
    const findByNameSpy = sinon.spy(Lib.prototype, 'findByName')
    const target = { value: "2486" }
    const app = shallow(<App />);

    app.setState({ transactions: fixture })

    app.find('input').simulate('change', { target })


    expect(handleSearchByNameSpy).to.have.been.called
    expect(findByNameSpy).to.have.been.calledWith(target.value)

})


it('should use fetch transaction on rest button', () => {
    const fetchTransactionsSpy = sinon.spy(App.prototype, 'fetchTransactions')
    const app = shallow(<App />);

    app.find('button').simulate('click')

    expect(fetchTransactionsSpy).to.have.been.called

})


beforeEach(() => {
    fetchStub = sinon.stub(global, 'fetch').returns(Promise.resolve({
        json: () => Promise.resolve(fixture)
    }))
});

afterEach(() => {
    fetchStub.restore()
});