import React from 'react';
import chai from 'chai';
import sinonChai from 'sinon-chai';
import chaiEnzyme from 'chai-enzyme';
import { configure, shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';
configure({ adapter: new Adapter() });
chai.use(sinonChai);
chai.use(chaiEnzyme());
global.React = React;
global.shallow = shallow;
global.render = render;
global.mount = mount;
global.sinon = sinon.createSandbox();

global.expect = chai.expect