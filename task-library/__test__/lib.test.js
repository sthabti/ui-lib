const Lib = require('../lib.js');
const fixtures = require('./fixture');

test('sortByAmount - a function that sorts an `Array` of transactions by `amount`', () => {

    const spy = jest.spyOn(Array.prototype, 'sort');
    const sut = new Lib(fixtures);
    const result = sut.sortByAmount();

    expect(spy).toHaveBeenCalled();

    for (let i = 1; i < result.length; i++) {
        const current = result[i].amount;
        const previous = result[i - 1].amount

        expect(current > previous).toBe(true)
    }

    spy.mockRestore();

});

test('findByName - a function that searches over `name` in an `Array` of transactions', () => {

    const spy = jest.spyOn(Array.prototype, 'filter');
    const name = 'Personal Loan Account 2486';
    const sut = new Lib(fixtures);
    const result = sut.findByName(name);

    result.map(item => expect(item.name).toBe(name));
    spy.mockRestore();


})