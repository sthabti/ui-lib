class Lib {
    constructor(transactions) {
        this.transactions = transactions;
    }

    sortByAmount() {
        return this.transactions.sort((one, two) => one.amount - two.amount)
    }

    findByName(name) {
        return this.transactions.filter((item) => item.name.toLowerCase().includes(name))
    }
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = Lib;
} else {
    window.Lib = Lib;
}